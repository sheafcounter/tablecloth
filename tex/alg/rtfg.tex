\chapter{Representation theory of finite groups}%
\label{cha:representation_theory_of_finite_groups}


\section{Groups}% \label{sec:groups}

\subsection{Overview}% \label{sub:overview}

We begin with an overview of various aspects of representation theory.
\begin{enumerate} \item First, the principle that any subject should be
    organized by its symmetries (harmonic analysis). We want to spot symmetries
    in a problem, and this is related to physics and QFT.  \item We want to
    consider symmetries related to subtle actions of space. For example, for a
    scheme $X$ and sufficiently nice group scheme $G$, we can consider the
    quotient stack $X / G$. The projection from $X$ is a principle $G$-bundle,
    and in fact, we can do this for $X = \operatorname{Spec} k$, which is a
    point. Also, we will need the idea of the fiber product $X \times_Z Y$
    later.  \item Groups are used to describe objects that are locally trivial
    but not globally (for example cohomology classes).  \item Groups can be
    recovered from their categories of representations via Tannaka-Krein
    duality by considering automorphisms of the forgetful functor $\Rep G \to
\ms{Vect}$.  \item The most important class of groups is the compact Lie
    groups.  \end{enumerate}

Recall the notion of a group action on a set $X$. If we view $X \in \mc{C}$ for
some category $\mc{C}$, then we want our group $G$ to act by automorphisms in
$\mc{C}$. Therefore we can define topological group actions on topological
spaces and actions of groups on vector spaces.

\begin{lem} For a group $G$ and vector space $V$, then an action of $G$ on $V$
is equivalent to a morphism $\pi: G \to GL(V)$.  \end{lem}

\begin{exm} If $G$ acts on a set $X$, then the space $\mc{O}(X) = \Hom(X,k)$ of
functions on $X$ is a representation of $G$.  \end{exm}

We can generalize the above example to where $X$ has additional structure,
where we can consider functions in various categories. For example, if we
consider $G = SL_2(\R)$ acting on the upper half plane $\mc{H}$, then we can
consider the space $\mc{O}_{\mc{H}}(\mc{H})$ of holomorphic functions on
$\mc{H}$.

\subsection{Category of representations}% \label{sub:category_of_representations}

\begin{lem} \begin{enumerate} \item Representations of $G$ on vector spaces
    over a field $k$ form a category $\Rep_G(k) = \Rep(G)$. Note that morphisms
    are \[ \Hom_{\Rep G}(U,V) = \qty{\varphi: U \to V \mid g_V \circ \varphi =
    \varphi \circ g_U \text{ for all } g \in G}. \] \item $\Rep_G(k)$ is an
    abelian category.

            Recall that an abelian category is a category with the following:
            \begin{itemize} \item A zero object $0$. Here, just take the zero
                representation.  \item A direct sum (coproduct) operation
                $\oplus$. Here, take $g(u \oplus v) = gu \oplus gv$.  \item
                Subobjects, which in this case are $G$-invariant subspaces.
            \item Quotients by subobjects. This is obvious.  \item $\Rep G$ has
                all (co)kernels and (co)images, and images and coimages are
                isomorphic.  \end{itemize} Some examples of abelian categories
                are categories of modules over a ring, but in fact $\Rep G$ is
                equivalent to $k[G]\text{-}\mathsf{Mod}$.  \item $\Rep G$ is a
                monoidal category. For module categories, this is only true in
                general for commutative rings. For two representations $U,V$ of
                $G$, we can define an action on $U \otimes V$ by $g(u \otimes
                v) = gu \otimes gv$.

            Recall that a monoidal structure on a category $\mc{C}$ is an
            associative and unital functor $\mc{C} \times \mc{C} \to \mc{C}$.
            The unit in $\Rep G$ is $k$ with the trivial action.  \item
            $\Rep(G)$ is a closed monoidal category. This means that there is
            an internal Hom functor \[ - \otimes -: \underline{\Hom} \colon
        C^{\mr{op}} \times C \to C \] that is the right adjoint to the tensor
product.  \end{enumerate} \end{lem}

\begin{exms} We give some examples of closed monoidal categories.
    \begin{enumerate} \item In the category of sets, we see that \[ \Hom(A
        \times B, C) \simeq \Hom(A, \Hom(B,C)) \] by the maps \[ f \coloneqq A
    \times B \to C \leftrightarrow (a \mapsto f(a,-)). \] \item In the category
of vector spaces, we again note that \[ \Hom(A \otimes B, C) \simeq \Hom(A,
\Hom(B,C)) \] by the same currying morphism.  \end{enumerate} \end{exms}

We can check that the internal Hom in the category of representations is simply
the vector space Hom. To see that this is a representation, define the action
$(g \varphi)(u) = g \varphi(g^{-1}u)$.

Recall that a right adjoint $G: B \to A$ to a functor $F:A \to B$ satisfies \[
\Hom_B(Fa, b) \simeq \Hom_A(a,Gb) \] as sets in a manner that is natural in
$a,b$. Being a closed monoidal category means that $- \otimes b$ is a left
adjoint to $\underline{\Hom}(b,-)$.

\subsection{Representations of finite groups}%
\label{sub:representations_of_finite_groups}

First we consider invariants of $V \in \Rep G$, which correspond to morphisms
from the trivial representation.

\begin{lem} We have the two identities $\Hom_G(k,V) = V^g$ and $\Hom_k(U,V)^G =
\Hom_G(U,V)$.  \end{lem}

\begin{defn} A representation $V \in \Rep G$ is \textit{irreducible} if $V \neq
0$ and has no nontrivial subrepresentations.  \end{defn}

Then define $\Irr_G$ to be the set of isomorphism classes of irreducible
representations of $G$. We will use the following strategy to understand $\Rep
G$: \begin{enumerate} \item Find all irreducible representations; \item Study
    each irreducible representation; \item For any interesting representation,
    decompose it into irreducibles.  \end{enumerate}

For a finite group $G$, consider the set of functions $\mc{O}(G) = \Hom(G,k)$.
Then we have two subrepresentations $\mc{O}_+(G) \coloneqq \mc{O}(G)^G$ and
$\mc{O}_-(G) = \qty{f: G \to k \mid \sum_{g \in G} f(g) = 0}$.

\begin{lem} If the characteristic of $k$ does not divide $\abs{G}$, then
$\mc{O}(G) \simeq \mc{O}_+(G) \oplus \mc{O}_-(G)$.  \end{lem}

\begin{proof} The dimensions match up, so the only constant satisfying
$c\abs{G} = 0$ is $c = 0$ by assumption.  \end{proof}

\begin{exm} If $G = S_2 = \qty{e,a}$, then $\mc{O}_{\pm}(S_2)$ are irreducible,
but they are actually the same subrepresentation of $\mc{O}(S_2)$ over $\F_2$.
\end{exm}

\begin{defn} A representation $V$ is \textit{semisimple} if it is a direct sum
of irreducibles.  \end{defn}

\begin{thm}[Schur Lemma] Let $U,V$ be two irreducible finite-dimensional
    representations of $G$.  \begin{enumerate} \item If $0 \neq \alpha \in
        \Hom_G(U,V)$, then $\alpha$ is an isomorphism; \item $\End_G(U)$ is a
        finite-dimensional division algebra; \item If $k = \ol{k}$, then
$\End_G(U) = k$.  \end{enumerate} \end{thm}

\begin{proof} \hfill \begin{enumerate} \item Note that $\ker \alpha$ is a
    subrepresentation and $U$ is irreducible, so $\ker \alpha = 0$. Similarly,
    the image is nonzero and is a subrepresentation of $V$, so it must be all
    of $V$.  \item Any nonzero endomorphism must be an isomorphism.  \item If
    $\alpha \in \End_G(U)$ is nonzero, it has an eigenvalue $\lambda$ with
    eigenvector $v$. Then $\ker(\alpha - \lambda)$ is a nonzero
    subrepresentation, so it must be everything.  \end{enumerate} \end{proof}

\begin{cor} \hfill \begin{enumerate} \item $V$ is semisimple if and only if it
    can be written as a direct sum \[ V = \bigoplus_{U \in \Irr(G)} M_U \otimes
U. \] \item Let $U$ be irreducible. Then $\Hom_G(U,V) = \Hom_G(U,U) \otimes
M_U$.  \end{enumerate} \end{cor}

\begin{proof} \hfill \begin{enumerate} \item Note that if $V$ is a direct sum
    of irreducibles, then we have \[ V = \bigoplus_{U \in\Irr(G)} \qty(
    \bigoplus_{i} U_i ) = \bigoplus U \otimes M_u. \] \item Hom is an additive
    functor, so just use Schur's Lemma.  \end{enumerate} \end{proof}

\begin{cor} If $k$ is algebraically closed, then $\Hom_G(U,V) = M_U$ for $V$
semisimple and $U$ irreducible.  \end{cor}

\begin{thm} Let $G$ be abelian and $k$ be algebraically closed. Then all
finite-dimensional irreducible representations of $G$ are one-dimensional.
\end{thm}

\begin{proof} Let $V \in \Irr(G)$ and $g \in G$. Then $g$ has an eigenvalue
    $\lambda$ with eigenvector $v$. Therefore, $\ker(g - \lambda) = V$ is a
    subrepresentation because $G$ is abelian, so all subspaces are invariant
    and thus $\dim V = 1$.  \end{proof}

This tells us that $\Irr(G) = \Hom(G, k^*) \eqqcolon \wh{G}$. This is known as
the group of \textit{characters}\footnote{Note there is another notion of
character that will appear in this course.} of $G$.

\begin{lem} Subrepresentations of semisimple representations are semisimple.
\end{lem}

\begin{proof} Let $V' \subseteq V$ be a subrepresentation of an irreducible
    representation. Then $V'$ has an irreducible subrepresentation $V_1'$ and
    thus $0 \neq \Hom(V_1', V) = \bigoplus_{i \in I} \Hom(V_1', V_i)$, so there
    exists $i$ such that $\Hom(V_1', V_i) \neq 0$, so $V_1' \subseteq V$. Then
    $V' / V_1' \subseteq V / V_1' = \bigoplus_{j \neq i} V_j$, so we have an
    exact sequence \[ 0 \to V_1' \to V' \to V'/V_1' \to 0. \] Therefore $V =
V_i \oplus C$, so $V' = (V_i \oplus C) \cap V'$.  \end{proof}

\begin{thm} \hfill \begin{enumerate} \item Sums of semisimple representations
    are semisimple.  \item $V$ is semisimple iff and only if for every
    subrepresentation $V' \subseteq V$, there is another subrepresentation
    $V''$ such that $V = V' \oplus V''$.  \end{enumerate} \end{thm}

\begin{proof} We will only prove the second part (the first part is obvious).
    Note that if the condition holds, then choose an irreducible
    subrepresentation $V'$, apply the condition, and use induction on the
    dimension.

    Now suppose $V$ is semisimple. Then apply Lemma 1.14 to $V' = \bigoplus
\Hom_G(U,V') \otimes U \subseteq \bigoplus \Hom_G(U,V) \otimes U$ and we see
that there is a complement.  \end{proof}

\begin{thm} Let $G$ be a final group and $k = \C$. Then every $V \in
\Irr^{\mr{fd}}(G)$ has a $G$-invariant Hermitian inner product and is
semisimple.  \end{thm}

\begin{proof} To construct an invariant inner product $(-,-)$, just take any
    Hermitian inner product $\ev{-,-}$ and define \[ (u,v) = \sum_{g \in G}
    \ev{gu,gv}. \] If we have an inner product, let $W \subseteq V$ be a
    subrepresentation. Then $W^{\perp}$ is also a subrepresentation, and
    clearly $W \oplus W^{\perp} = V$, so $V$ is semisimple by Theorem 1.15.
\end{proof}

\begin{thm}[Maschke] If $\Char k \nmid \abs{G}$, then all finite-dimensional
representatons of $G$ are semisimple.  \end{thm}

We will now study in detail the characters $\Hom(G, k^*)$ of $G$.

\begin{lem} \hfill \begin{enumerate} \item $\Hom(G, k^*)$ is a group with
operation the multiplication of functions.  \item $\Hom(G, k^*) =
\Irr^{\mr{1d}}(G)$ because $GL_1(k) = k^*$.  \item $\Hom(G, k^*) =
\Hom(G^{\mr{ab}}, k^*)$.  \end{enumerate} \end{lem}

If $A$ is abelian, then we call the group of characters $\wh{A} \coloneqq
\Hom(A, k^*)$ the \textit{Pontryagin dual of $A$}.

\begin{exms} \hfill \begin{enumerate} \item Note that $\wh{\Z} = k^*$ because
    $\mathsf{Ab} = \Z\text{-}\mathsf{Mod}$.  \item We can see that $\wh{\Z/n\Z}
    = \mu_n(k)$.  \item If $k$ is algebraically closed, and $\Char k \nmid n$,
    then $\wh{\Z/n\Z} \simeq \Z/n\Z$.  \end{enumerate} \end{exms}

Note that $\Hom(G, k^*) \times \Hom(H, k^*) \cong \Hom(G \times H, k^*)$ by the
categorical properties of the product. Consider the functor $\mathsf{Ab^{op}}
\to \mathsf{Ab}$ given by $A \mapsto \wh{A} = \Hom(A, k^*)$. Note that there is
a natural\footnote{Yes, a natural transformation.} morphism $\iota: A \to
\wh{\wh{A}}$ given by $\iota(g)(\chi) = \chi(g)$ for $g \in A$.

\begin{lem}[Pontryagin Duality] If $k$ is an algebraically closed field of
    characteristic $0$, then $\wh{-}: \mathsf{(Ab^f)^{op}} \to \mathsf{Ab^f}$
    is an equivalence of categories. In fact, the natural transformation
    $\iota$ defined above is an isomorphism.  \end{lem}

\begin{proof} This is obviously true for finite cyclic groups, and then use the
structure theorem for finite abelian groups and the properties of the product.
\end{proof}

\begin{rmk} If $\Char k > 0$, then the result extends to finite abelian group
schemes. If we take locally compact abelian groups and $\Hom(A, S^1)$, then
this result still holds.  \end{rmk}

Now we will study representations of products of groups. Consider the box
product \[ -\boxtimes-: \Rep G \times \Rep H \to \Rep(G \times H) \] with the
operation the tensor product with $G$ acting from the left and $H$ acting from
the right.

\begin{lem} Let $W \in \Irr(G \times H)$. Then there exist $V \in \Irr(G)$ and
$U \in \Irr(H)$ such that $W = V \boxtimes U$.  \end{lem}

\begin{proof} Let $V \in \Rep G$. Then consider the morphism $ev: V \otimes
    \Hom_G(V, W) \to W$. We claim this is a representation of $H$. To see this,
    define $(h \varphi)(v) = (1,h)(\varphi v)$, which commutes with the action
    of $G$ on $W$. Clearly, evaluation is surjective, so we can consider
    $\ker(ev) = V \otimes K'$ for some representation $k'$ of $H$. Therefore we
    can write \[ W = \frac{V \otimes \Hom(V, W)}{\ker(ev)} = V \otimes
    \frac{\Hom(V, W)}{K'} = V \otimes H' \] for some $H' \in \Irr(H)$ (if $H$
is reducible, then so is the entire representation).  \end{proof}

\begin{lem} If $V \in \Irr G$ and $U \in \Irr H$, then $V \boxtimes U \in \Irr
G \times H$.  \end{lem}

\begin{proof} Consider an irreducible subrepresentation $V' \boxtimes U'$. Then
    \[ 0 \neq \Hom_{G \times H}(V' \boxtimes U', V \boxtimes U) = \Hom_G(V',V)
    \otimes \Hom_H(U', U), \] so $V' = V$ and $U' = U$.  \end{proof}

\begin{thm} The box product gives an isomorphism $\Irr G \times \Irr H
\xrightarrow{\boxtimes} \Irr G \times H$.  \end{thm}

Now let $V$ be a representation of $G$ and consider the \textit{matrix
coefficient} map \[ c^V: V \otimes V^k \to \mc{O}(G), \hspace{1cm} c^V_{u
\otimes \alpha}(g) = \gen{gu, \alpha}. \] This is equivariant with respect to
$G^2$, where $(g,h)f(x) = f(h^{-1}xg)$. Also, the matrix coefficient map $c^V:
V \otimes V^* \to \mc{O}(G)$ is an embedding because $V, V^*$ are both
irreducible.

\begin{cor} Any finite group $G$ has finitely many irreducible representations.
\end{cor}

\begin{thm} Let $k$ be an algebraically closed field of characteristic $0$.
    Then \[ \mc{O}(G) \cong \bigoplus_{V \in \Irr G} V^* \boxtimes V \] as a
representation of $G^2$ \end{thm}

\begin{proof} We compute that as representations of $1 \times G$, \[ \Hom_{1
\times G}(V, \mc{O}(G)) \otimes V = \Hom_1(V, k) = V^* \] by Frobenius
reciprocity.  \end{proof}

It is not hard to see that an explicit isomorphism can be given by $c^V$, which
is the same thing as evaluation.

\subsection{Characters}% \label{sub:characters}

From now on, we will always work over the complex numbers. It turns out that
each irreducible representation of $G$ gives us one conjugation-invariant
function $G \to \C$, the trace. Denote the \textit{character} of a
representation $V$ by $\chi_V(g) \coloneqq \Tr_V g$.

\begin{lem} \hfill \begin{enumerate} \item $\chi_{U \oplus V} = \chi_U +
\chi_V$; \item $\chi_{U \otimes V} = \chi_U \cdot \chi_V$.  \end{enumerate}
\end{lem}

These results follow from linear algebra of direct sums and tensor products.

\section{Representation theory of the symmetric group}%
\label{sec:representation_theory_of_the_symmetric_group}

Now we will construct irreducible representations of the symmetric group.
Because irreducible representations correspond to conjugacy classes, there are
bijections between the following: \begin{enumerate} \item Irreducible
    representations of $S_n$; \item Conjugacy classes of $S_n$; \item
    Partitions $\lambda$ of $n$; \item Young diagrams with $n$ squares; \item
    Conjugacy classes of nilpotent matrices of size $n$; \item Finite (length
    $n$) subschemes of $\A^2$.  \end{enumerate}

We want to construct an irreducible representation for each partition
$\lambda$. For a partition $\lambda = (\lambda_1, \ldots, \lambda_m)$, write
$S_{\lambda} = S_{\lambda_1} \times \cdots \times S_{\lambda_m}$. We will write
$M^{\lambda} = \operatorname{Coind}_{S_{\lambda}}^{S_n} \tau_{\lambda}$, where
$\tau$ is the trivial representation. Similarly, define $N_{\lambda} =
\operatorname{Coind}_{S_{\lambda}}^{S_n} \sigma_{\lambda}$, where $\sigma$ is
the sign representation.

\begin{thm} If $C, R$ are conjugate partitions, then there exists a unique
irreducible representation $\pi_C$ that is common to $M^R$ and $N^C$.
\end{thm}

Now, we will consider the \textit{dominance order} on the set $\Pi_n$ of
partitions of $n$. We say that $\lambda \geq \mu$ if $\lambda_1 \geq \mu_1$,
$\lambda_1 + \lambda_2 \geq \mu_1 + \mu_2$, and so on.

We may also consider a more geometric notion. Let $\mc{N}_n$ be the cone of
nilpotent $n \times n$ matrices. Then every nilpotent matrix has a Jordan
canonical form that corresponds to a shift operator $e_{\lambda}$ (shift left
on a Young diagram $\lambda$). Then $\mc{N}_n$ is a stratified space with
strata given by orbits $\mc{O}_{\lambda}$ corresponding to partitions
$\lambda$. Then we say that $\lambda \geq \mu$ if $\ol{\mc{O}}_{\lambda}
\supseteq \mc{O}_{\mu}$.

\begin{lem} The dominance and closure orders coincide.  \end{lem}

\subsection{Geometry of $GL_n$}% \label{sub:geometry_of_gl_n_}

The correspondence between irreducible representations of $S_n$ and orbits of
nilpotent matrices under conjugation by $GL_n$, due to Springer, is the origin
of \textit{geometric representation theory}. This also extends to other
reductive algebraic groups. For this, we will need to do some geometry.

First recall the \textit{Grassmannian} $Gr_p(V)$ which parameterizes dimension
$p$ subspaces of $V$. Then we can consider generalized flag varieties $Gr_{1
\leq p_1 \leq \cdots \leq p_k \leq n}$, which parameterizes flags of subspaces
with dimension $p_1, \ldots, p_n$. Finally, we have the \textit{flag variety}
\[ \mc{F} = Gr_{1<2<\ldots<n} = \qty{U_1 \subset U_2 \subset \cdots \subset U_n
= V \mid \dim U_i = i}. \]

\begin{lem} These spaces are all homogeneous spaces of $GL_n$ which are smooth
projective varieties.  \end{lem}

Now recall that the nilpotent cone $\mc{N}_n = \bigsqcup_{\lambda \in \Pi_n}
\mc{O}_{\lambda}$ is a disjoint union of smooth pieces. By definition of the
dominance order, we know that \[ \ol{\mc{O}}_{\lambda} = \bigcup_{\mu \leq
\lambda} \mc{O}_{\mu}. \] However, note that $\mc{N}_n$ itself is a singular
space, so we should be able to extract information from the singularities.

\begin{defn} Let $X$ be a singular variety. Then a \textit{resolution} of $X$
is a proper birational map $\pi: \wt{X} \to X$ from a smooth variety $\wt{X}$.
\end{defn}

\begin{exm} We can resolve the variety $(xy = 0) \subseteq \C^2$ by taking the
disjoint union of two lines.  \end{exm}

If we consider $\mc{N}_2$, then this is just a quadric cone in $\A^3$, so we
can resolve by blowing up the origin to get $\wt{\mc{N}}_2 = \qty{(e,L) \in
\mc{N}_2 \times \P^1 \mid e \in L}$.

\begin{lem} $\mc{F} = GL_n / B_0$, where $B_0 \subseteq GL_n$ is the subgroup
    of upper-triangular matrices, also called the \textit{standard Borel}. In
    addition, $\mc{F}$ has a natural identification with the space of Borel
    subgroups of $GL_n$.  \end{lem}

\begin{rmk} Note that $GL_n$ is a nonabelian reductive group. $G$ has a maximal
    Borel $B_0$, and we call the subgroup of $B_0$ with $1$s on the diagonal
    $N_0$. Finally, we write $T_0 = B_0 / N_0$, and this is called the
    \textit{standard Cartan}, or the \textit{maximal torus}, because $T_0
    \simeq \mathbb{G}_m^n$.  \end{rmk}

\subsection{Springer theory for $GL_n$}% \label{sub:springer_theory_for_gl_n_}

We want to resolve $\mc{N}_n$ in general. To do this, we will consider $\mf{g}
= M_n(\C) = \mf{gl}_n$. Then define \[ \wt{\mf{g}} = \qty{(x,F) \in \mf{g}
\times \mc{F} \mid x F_i \subseteq F_i}, \] so we have a natural morphism $\pi:
\wt{\mf{g}} \to \mf{g}$ that forgets the flag. Similarly, define \[ \wt{\mc{N}}
= \qty{(x,F) \in \mc{N} \times \mc{F} \mid x F_i \subseteq F_{i-1}}. \] Again,
there is a map $\pi: \wt{\mc{N}} \to \mc{N}$. Write $\mc{F}_x = \pi^{-1} x$.

\begin{lem} Let $s \in \mf{g}_{rs}$ be a \textit{regular semisimple} matrix. In
other words, $s$ has $n$ distinct eigenvalues. Then $\abs{\mc{F}_s} = n!$ and
$\mc{F}_s$ is a torsor for $S_n$.  \end{lem}

Here are some examples of torsors: \begin{enumerate} \item The set of bases of
    $\C^n$ is a torsor for $GL_n$.  \item The M\"obius strip with the zero
    section removed is a torsor for $\R^*$.  \item Rank $n$ vector bundles over
    a base space $B$ are the same thing as $GL_n$-torsors over $B$.
    \end{enumerate}

If $G$ acts on $X$ and $Y$, we can create a new $G$-set $X \times_G Y \coloneqq
(X \times Y) / \Delta G$, where the diagonal action of $G$ is $g(x,y) =
(xg^{-1}, gy)$. We can see that $X \times_G Y$ is a $Y$-bundle over $X / G$, so
if $X$ is a $G$-torsor, then $X \times_G Y \simeq Y$.

Note that the set of regular semisimple matrices is a Zariski-open subset of
$\mf{g}$. We now consider the special fibers: \begin{enumerate} \item Above $x
    = 0$, the fiber is simply all of $\mc{F}$.  \item If $x$ corresponds to the
    Young diagram $\ydiagram{5}$, then $\mc{F}_x$ is a point.  \item For $n =
    3$, the fiber above the Young diagram $\ydiagram{2,1}$ is two copies of
    $\P^1$ intersecting at a point.  \end{enumerate}

\begin{lem} As spaces, $\wt{\mf{g}} = G \times_B \mf{b}$. This means that
$\wt{\mf{g}}$ is a vector bundle over $G/B = \mc{F}$. Because $\mf{b}$ is a
representation of $B$, $H^0(G/B, \wt{\mf{g}})$ is a representation of $G$.
\end{lem}

\begin{rmk} These \textit{Associated Bundle} constructions allow us to
    construct $G$-equivariant vector bundles from representations of $B$. If we
    consider the character $(m,n)$ that sends $(\C^*)^2 \ni (a,b) \mapsto a^m
    b^n$ as a representation $\C_{m,n}$ of $B$, then we can define the vector
    bundle $V_{m,n} = G \times_B \C_{m,n}$. Then we can consider the
    representation $L_{m,n} = H^0(G/B, V_{m,n})$. Then each $L_{m,n}$ is either
    zero or irreducible or zero and all irreducible representations of $G$ are
    of this type. This is the \textit{Borel-Weil theorem}.  \end{rmk}


\begin{exm} For $G = GL_2$, then it is easy to see that $G / B = \P^1$.
\end{exm}

\begin{lem} As spaces, $\wt{\mc{N}} = G \times_B \mf{n} = T^* \mc{F}$.
\end{lem}

\begin{lem} If $G$ acts on $X$, then any $G$-equivariant map $X \to G/A$
identifies $X$ with an associated bundle $G \times_A X_{eA}$.  \end{lem}

Note that there is a natural symplectic structure on $T^* \mc{F}$ and that
$\wt{\mc{N}} \to \mc{N}$ is a resolution. Here, over a regular nilpotent
matrix, the map is an isomorphism and the map is proper because fibers $\mc{F}$
is projective.

We will call the partition $(n-1,1)$ \textit{subregular} and the partition $2,
1^{n-2}$ minimal. In general, these classes exist for all semisimple Lie
algebras and we can perform the Springer construction for all of them. We now
guess that $\dim \pi_{\lambda}$ is the number of irreducible components of
$\mc{F}_{\lambda}$.

\begin{rmk} Semisimple Lie algebras are the same thing as Dynkin diagrams,
    which are displayed below: \begin{align*} A_n = \dynkin A{} & & E_7 =
        \dynkin E7 \\ D_n = \dynkin D{} & & E_8 = \dynkin E8 \\ E_6 = \dynkin
        E6  & &  \\ B_n = \dynkin B{} & & G_2 = \dynkin G2 \\ C_n = \dynkin C{}
            & & F_4 = \dynkin F4 \end{align*} \end{rmk}

\begin{thm} All irreducible components of Springer fibers are equidimensional.
\end{thm}

We now guess that $\pi_{\lambda} = \C[\Irr(\mc{F}_{\lambda})]$. However, it is
not clear what the action $S_n$ is. For this, we will need a continuity
argument. For this, we will need sheaf cohomology.

\begin{thm} $\mc{F}_{\lambda}$ is paved. This means that it is a disjoint union
of affine spaces.  \end{thm}

Being paved means that it is easy to compute the cohomology with coefficients
in $\Z$.

\subsection{Sheaves}% \label{sub:sheaves}

A \textit{presheaf} on a space $X$ valued in a category $\mc{C}$ is a presheaf
$X^{\mr{op}} \to \mc{C}$. A presheaf $\mc{F}$ is a \textit{sheaf} if for an
open set $U$ and a cover $U_i$ with sections $f_i \in \mc{F}|_{U_i}$ such that
$f_i |_{U_i \cap U_j} = f_j |_{U_i \cap U_j}$, then there exists a unique
section $f \in \mc{F}|_{U}$ such that $f|_{U_i} = f_i$.

Let $f:X \to Y$ be a morphism of spaces. Then there is a \textit{pushforward}
\[ (f_* \mc{F}) (V) = \mc{F}(f^{-1} V) \] and a \textit{pullback} \[ (f^*
\mc{G} (U)) = \lim_{\substack{\gets \\ f(U) \subseteq V}} \mc{G}(V). \] There
operations define functoriality for categories of sheaves.

If $\mc{C}$ is an abelian category, then so is $Sh(X, \mc{C})$, so we may
consider complexes of sheaves. Then we may consider the derived category of
complexes. In this setting, we have derived versions of $f_*, f^*$ as well as
the compactly supported $Rf_!$ and exceptional inverse image functor $Rf^!$.

Given a sheaf $\mc{F}$ and a point $x$, the \textit{stalk} $\mc{F}_x$ is
defined as the colimit \[ \mc{F}_x = \lim_{\substack{\to \\ x \in U}}
\mc{F}|_{U}. \] Alternatively, given the inclusion $\iota: x \to X$, the stalk
is simply $\iota^* \mc{F}$. Then we can define the \textit{support} of a sheaf
as the closure of the set of points with nonzero stalks.

\begin{rmk} The derived category is not an abelian category, but it is an
    \textit{triangulated category}. This means we have exact triangles \[ A \to
    B \to C \to A[1]. \] \end{rmk}

There is a \textit{Verdier duality} functor
$\mb{D}_X: D(X) \to D(X)^{\mr{op}}$ such that $\mb{D}f_* \mb{D} = f_!$ and
$\mb{D} f^* \mb{D} = f^!$.

\begin{exm} If $a: X \to \mr{pt}$, then $a^* \mc{F} = \mc{F}_x$ and $a^! k =
\omega_X$, the dualizing sheaf.  \end{exm}

Under base change, we have the standard push-pull trick, and if $f$ is proper,
then $f_! = f_*$. If $f$ is a smooth morphism, then $f^! = f^* [d_y - d_x]
\otimes \mr{or}_f$, where $\mc{or}$ is the orientation sheaf.

Note that cohomology is simply the derived functor of global sections, so we
will treat everything as a derived functor.

\begin{defn} A \textit{local system} on $X$ is a locally constant sheaf, and a
sheaf $\mc{F}$ is \textit{constructible} if there exists a stratification $X =
\bigsqcup S_i$ such that $\mc{F}|_{S_i}$ is a local system.  \end{defn}

\begin{rmk} All of the derived category and functoriality constructions apply
in the case of constructible sheaves.  \end{rmk}

\subsection{Springer fibers again}% \label{sub:springer_fibers_again}

Returning to our representation theory, we want to produce $a_* a^! \C$ from
our setup. Therefore, we will push the constant sheaf $\C$ forward from
$\wt{\mf{g}}$ to $\mf{g}$. The resulting complex of sheaves is called the
\textit{Grothendieck sheaf}.

\begin{rmk} Note that $S_n$ is embedded in $GL_n$ and is embedded in the
normalizer of the standard Cartan. In addition, $S_n \cong N_G(T) / T$.
\end{rmk}

If $\pi: \mf{\wt{g}} \to \mf{g}$ is the projection that forgets the flag,
denote the Grothendieck sheaf by $\mc{G} \coloneqq \pi_* k$. Then the stalk
$\mc{G}_x$ is the same as the cohomology $H^*(\mc{F}_x)$. We know that $S_n$
acts on $\mc{G}_{\mr{rs}} = \mc{G} | _{\mf{g}_{\mr{rs}}}$, so we need to extend
this action. 

\begin{prop} $\mc{G}$ is obtained from its restriction to the regular
    semisimple matrices by a procedure called ``intersection cohomology
    extension,'' and this works because $\pi$ is ``small,'' which means that
    the fibers increase slowly in some sense.  \end{prop}

\begin{defn} A morphism $\pi: Y \to X$ is \textit{semi-small} if $d_x = \dim
    \pi^{-1} x$ is generically zero and the closed set where $d_x \geq k$ has
    codimension at least $2k$. The morphism $\pi$ is \textit{small} if it is
    semi-small and the set of points with $d_x > 0$ has codimension at least
    $3$.  \end{defn}

\begin{thm} The morphism $\pi: \wt{\mf{g}} \to \mf{g}$ is small.  \end{thm}

This means that $S_n$ acts on the entire Grothendieck sheaf $\mc{G}$, so in
particular, it acts on the stalks. This tells us that each stalk is a
representation of $S_n$.

\begin{lem} There is an isomprhism $\C[S_n] \simeq \End(\mc{G}) =
H_{\mr{top}}(\wt{mf{g}} \times_{\mf{g}} \wt{\mf{g}})$.  \end{lem}

We conclude from our geometric construction that \begin{enumerate} \item
    Interesting objects have geometric realizations.  \item Geometric methods
    then give construction of irreducible representations.  \item This allows
    us to study $\Irr$ more efficiently.  \end{enumerate}

