# Tablecloth

This is the source code for my project *A finitely large tablecloth*. This
project was initiated as a joke when I realized that I had more total
pages of live-TeXed notes than Evan Chen has pages in all of his book *An
infinitely large napkin*. Because I am not Evan Chen, I do not plan to
spend a ton of effort on this besides the initial (hopefully under 24
hours) spent cleaning up the exposition and making all of the code
compile. Here are some more differences between this and *Napkin*:

- While Evan is attempting to show a mathematically advanced high school
  student what advanced mathematics looks like, *Tablecloth* is aimed at
  giving reasonably advanced undergraduates an overview of things they
  will need to learn in graduate school if they choose to study algebraic
  geometry and/or representation theory. In particular, prerequisites for
  *Tablecloth* include a reasonable undergraduate algebra course (for
  example, the first semester of graduate algebra at UMass Amherst), a
  reasonable undergraduate analysis course (just the first semester of
  undergraduate analysis at UMass will suffice), and a working
  understanding of point-set topology (for this, the first semester of
  graduate topology at UMass is definitely overkill).
- I make no attempt at being pedagogically correct. In particular,
  *Tablecloth* is simply a reorganization of my notes from various courses
  and seminars that I attended while at UMass and Columbia. These notes
  are a transcription of what actually happened during lecture to the best
  of my abilities, and so the idiosyncracies of both the lecturers and me
  will remain in *Tablecloth*. If you want to read my notes as they were
  actually taken, here are the links to the
  [notes](https://math.columbia.edu/~plei/notes.html) and the [source
  code](httpsL://git.snopyta.org/abstract/course-notes).

There is a similarity between *Tablecloth* and *Napkin*: both books show
the reader how much mathematics there is out there. If you consider that
all of the material in *Tablecloth* comes from courses and seminars that I
actually attended over the course of four years (two at UMass and two at
Columbia), this gives a reasonable account of the amount of work that
someone who gets into and attends an elite grad school in pure mathematics
does (minus independent reading research, and all that other stuff).

## Acknowledgements

First of all, I would like to thank Rafay Ashary and Matthew Hase-Liu for
suggesting that I do this as a joke and coming up with the name
(tablecloths are usually larger than napkins, and I have more content than
Evan does).

I would like to acknowledge all of those people who taught courses or gave
lectures in seminars: Anna Abasheva, Mohammed Abouzaid, R. Inanc Baykur,
Kevin Chang, Kuan-Wen Chen, Sam Dehority, Johan de Jong, Paul Gunnells,
Paul Hacking, Sebastian Haney, Caleb Ji, Cailan Li, Chao Li, Francesco
Lin, Eyal Markman, Alvaro Martinez, Ivan Mirkovic, Joaquin Moraga, Andrei
Okounkov, Morena Porzio, Giulia Sacca, Luca Schaffler, Che Shen, Eric
Sommers, Mike Sullivan, Jenia Tevelev, Eric Urban, Nicolas Vilches, Siman
Wong, Alex Xu, Haodong Yao, Robin Young, Avi Zeff, Ivan Zelich, Fan Zhou,
and Baiqing Zhu. 

I would also like to acknowledge Arthur Wang for providing notes when I
was not in class and Kuan-Wen Chen, Caleb Ji, Che Shen, Nicolas Vilches,
and Alex Xu for pointing out mistakes and typos in my notes.

Finally, I would like to thank the following people for their
interventions in my mathematical journey:

- All of my classmates, instructors, and counselors/assistant
  instructors/etc at MathILy and PROMYS, and in particular sarah-marie
  belcastro: for showing me that mathematics is a deep and beautiful
  subject.
- Ivan Mirkovic and Luca Schaffler: for showing me the beauty of
  representation theory and algebraic geometry, respectively.
- Paul Hacking: for introducing me to mirror symmetry, and for always
  being my advocate.
