# LICENSE

- The text is licensed under [CC-BY-SA 
  4.0](https://creativecommons.org/licenses/by-sa/4.0/).
- The source is licensed under [GNU GPL 
  v3](https://choosealicense.com/licenses/gpl-3.0/).
